import java.io.IOException;
import java.util.Scanner;
import javafx.beans.binding.Bindings;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		printBoard(board);
                
                boolean check = true; // For the while loop used, if the entry is occupied.
                boolean gameOver = true; 
                int numberofMoves = 0; // 
                
                while (gameOver){
                    
               //----------------------------------------------------------------------------------------
		System.out.print("Player 1 enter row number:");
		int row = reader.nextInt();
		System.out.print("Player 1 enter column number:");
		int col = reader.nextInt();
                if(row < 0 || row > 3 || col < 0 || col > 3){
                    System.out.println("You have entered coordinates outside of the board. Please try again.");
                    boolean x = true;
                    while(x){ 
                        System.out.print("Player 1 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 1 enter column number:");
                        col = reader.nextInt();
                        if(row < 0 || row > 3 || col < 0 || col > 3)
                            System.out.println("You have entered coordinates outside of the board. Please try again.");
                        if(row > 0 && row < 4 && col > 0 && col < 4)
                            x = false;
                        
                    }
                }
                if(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){ //If the location is occupied I want a new code.
                     System.out.println("This place is full. Please enter new row and column number");
                     while(check){
                         System.out.print("Player 1 enter row number:");
                         row = reader.nextInt();
                         System.out.print("Player 1 enter column number:");
                         col = reader.nextInt();
                        if(row < 0 || row > 3 || col < 0 || col > 3)
                            System.out.println("You have entered coordinates outside of the board. Please try again.");
                        if((row > 0 && row < 4 && col > 0 && col < 4) &&(board[row - 1][col - 1] != 'O' && board[row - 1][col - 1] != 'X'))
                            check= false;
                     }
                }
                check = true;
		board[row - 1][col - 1] = 'X';
                    
		printBoard(board);
                if(checkboard(board)){
                        System.out.println("X is win.");
                        break;
                    }
                numberofMoves++;
                    if(numberofMoves == 9){
                        System.out.println("The game ended in a draw.");
                        break;
                                
                    }
                //------------------------------------------------------------------------------------------------
		System.out.print("Player 2 enter row number:");
		row = reader.nextInt();
		System.out.print("Player 2 enter column number:");
		col = reader.nextInt();
                if(row < 0 || row > 3 || col < 0 || col > 3){
                    System.out.println("You have entered coordinates outside of the board. Please try again.");
                    boolean x = true;
                    while(x){ 
                        System.out.print("Player 2 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 2 enter column number:");
                        col = reader.nextInt();
                        if(row < 0 || row > 3 || col < 0 || col > 3)
                            System.out.println("You have entered coordinates outside of the board. Please try again.");
                        if(row > 0 && row < 4 && col > 0 && col < 4)
                            x = false;
                        
                    }
                }
                if(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){ //If the location is occupied I want a new code.
                     System.out.println("This place is full. Please enter new row and column number");
                     while(check){
                         System.out.print("Player 2 enter row number:");
                         row = reader.nextInt();
                         System.out.print("Player 2 enter column number:");
                         col = reader.nextInt();
                        if(row < 0 || row > 3 || col < 0 || col > 3)
                            System.out.println("You have entered coordinates outside of the board. Please try again.");
                        if((row > 0 && row < 4 && col > 0 && col < 4) &&(board[row - 1][col - 1] != 'O' && board[row - 1][col - 1] != 'X')){
                            check= false;
                                     
                        }
                     }
                }
                check = true;
                
                board[row - 1][col - 1] = 'O';
		printBoard(board);
                if(checkboard(board)){
                        System.out.println("O is win.");
                        break;
                    }
                numberofMoves++;
                
                }
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}
       	} 
        
        public static boolean checkboard(char[][] board){
            
            
            
            if(board [0][0] == 'X' && board [0][1] == 'X' && board [0][2] == 'X')
                return true; 
            else if(board [0][0] == 'O' && board [0][1] == 'O' && board [0][2] == 'O')
                return true; 
            else if(board [1][0] == 'X' && board [1][1] == 'X' && board [1][2] == 'X')
                return true; 
            else if(board [1][0] == 'O' && board [1][1] == 'O' && board [1][2] == 'O')
                return true; 
            else if(board [2][0] == 'X' && board [2][1] == 'X' && board [2][2] == 'X')
                return true; 
            else if(board [2][0] == 'O' && board [2][1] == 'O' && board [2][2] == 'O')
                return true; 
            else if(board [0][0] == 'X' && board [1][0] == 'X' && board [2][0] == 'X')
                return true; 
            else if(board [0][0] == 'O' && board [1][0] == 'O' && board [2][0] == 'O')
                return true; 
            else if(board [0][1] == 'X' && board [1][1] == 'X' && board [2][1] == 'X')
                return true; 
            else if(board [0][0] == 'O' && board [1][1] == 'O' && board [2][1] == 'O')
                return true; 
            else if(board [0][2] == 'X' && board [1][2] == 'X' && board [2][2] == 'X')
                return true; 
            else if(board [0][2] == 'O' && board [1][2] == 'O' && board [2][2] == 'O')
                return true; 
            else if(board [0][0] == 'X' && board [1][1] == 'X' && board [2][2] == 'X')
                return true; 
            else if(board [0][0] == 'O' && board [1][1] == 'O' && board [2][2] == 'O')
                return true; 
            else if(board [0][2] == 'X' && board [1][1] == 'X' && board [2][0] == 'X')
                return true; 
            else if(board [0][2] == 'O' && board [1][1] == 'O' && board [2][0] == 'O')
                return true; 
            return false;
        }

     
}
